FROM loadimpact/k6

COPY ./test.js /
COPY ./nouns.json /
COPY ./adjectives.json /

WORKDIR /

CMD ["run", "test.js"]
