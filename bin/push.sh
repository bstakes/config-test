#!/usr/bin/env bash

IMAGE=config-test
VERSION=$(awk '/current/{ print $3 }' .bumpversion.cfg)

docker tag $IMAGE:$VERSION artifactory.myomnicell.com/nextgen-docker-user/$IMAGE:$VERSION
docker push artifactory.myomnicell.com/nextgen-docker-user/$IMAGE:$VERSION
