#!/usr/bin/env bash

IMAGE=config-test
VERSION=$(bump2version patch --list | grep new | sed 's/^.*=//g')

echo "building docker image at version: $VERSION"
docker build -t $IMAGE:$VERSION .

